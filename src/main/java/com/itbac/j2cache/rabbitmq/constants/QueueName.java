package com.itbac.j2cache.rabbitmq.constants;

/**
 * @description 交换机名称
 *
 * @author itbac
 * @email 1218585258@qq.com
 * @date 2020/6/17
 */
public interface QueueName {

    public static final String QUEUE_NAME_PREFIX = "j2cacheNotice.queue.";

}
