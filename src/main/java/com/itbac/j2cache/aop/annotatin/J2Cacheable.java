package com.itbac.j2cache.aop.annotatin;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description 缓存注解，模仿Spring Cacheable
 * 从缓存中获取数据，或者 从DB查询后，设置到缓存。
 *
 * @author itbac
 * @email 1218585258@qq.com
 * @date 2020/6/17
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface J2Cacheable {

    String cacheName() default ""; //缓存名称

    String cacheKey(); //缓存key

    int expire() default 3600; //缓存有效期（单位：秒），默认1小时。

    int reflash() default -1; //缓存主动刷新时间（单位：秒）,默认-1表示不主动刷新。

    //value 缓存数据类型，不是String 的，需要转换成String
    Class clazz() default String.class;

}
